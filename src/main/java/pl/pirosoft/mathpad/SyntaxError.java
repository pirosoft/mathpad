package pl.pirosoft.mathpad;

public class SyntaxError {
    private final int charPositionInLine;
    private final String message;

    SyntaxError(int charPositionInLine, String message) {
        this.charPositionInLine = charPositionInLine;
        this.message = message;
    }

    public int getCharPositionInLine() {
        return charPositionInLine;
    }

    public String getMessage() {
        return message;
    }
}
