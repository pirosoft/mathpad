package pl.pirosoft.mathpad;

import org.antlr.v4.runtime.tree.TerminalNode;

public class MissingVariableException extends StatementException {
    private final String variableName;

    public static MissingVariableException fromVarId(TerminalNode varId) {
        String variableName = varId.getText();
        int charPositionInLine = varId.getSymbol().getCharPositionInLine();
        return new MissingVariableException(variableName, charPositionInLine);
    }

    MissingVariableException(String variableName, int charPositionInLine) {
        super("Unknown variable " + variableName, charPositionInLine);
        this.variableName = variableName;
    }

    public String getVariableName() {
        return variableName;
    }
}
