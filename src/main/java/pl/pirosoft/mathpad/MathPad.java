package pl.pirosoft.mathpad;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.Scanner;

public class MathPad {

    Scanner scanner = new Scanner(System.in);
    ScriptVisitor scriptVisitor = new ScriptVisitor();

    public static void main(String[] args) {
        new MathPad().loop();
    }

    private void loop() {
        while (true) {
            System.out.print(">>> ");
            String line = scanner.nextLine();
            if (line.isEmpty()) {
                break;
            }
            try {
                System.out.println(line + " --> " + execute(line));
            } catch (StatementException ex) {
                System.out.println("    Pos | Error ");
                ex.getErrors().forEach(error -> {
                    System.out.printf("   %4d : %s\n", error.getCharPositionInLine(), error.getMessage());
                });
            }
            System.out.println();
        }
    }

    private Double execute(String line) {
        ErrorCollector errorCollector = new ErrorCollector();
        Double result = scriptVisitor.visitScript(parse(line, errorCollector));
        if (!errorCollector.getSyntaxErrors().isEmpty()) {
            throw new StatementException(errorCollector.getSyntaxErrors());
        }
        return result;
    }

    private MathPadParser.ScriptContext parse(String line, ErrorCollector errorCollector) {
        MathPadLexer lexer = new MathPadLexer(CharStreams.fromString(line));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MathPadParser parser = new MathPadParser(tokens);
        lexer.removeErrorListeners();
        lexer.addErrorListener(errorCollector);
        parser.removeErrorListeners();
        parser.addErrorListener(errorCollector);

        return parser.script();
    }
}
