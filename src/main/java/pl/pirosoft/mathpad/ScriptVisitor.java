package pl.pirosoft.mathpad;

import java.util.HashMap;
import java.util.Map;

public class ScriptVisitor extends MathPadBaseVisitor<Double> {

    private final Map<String, Double> vars = new HashMap<>();

    @Override
    public Double visitAssignment(MathPadParser.AssignmentContext ctx) {
        String varName = ctx.ID().getText();
        Double varValue = visit(ctx.expression());
        vars.put(varName, varValue);
        return varValue;
    }

    @Override
    public Double visitExpression(MathPadParser.ExpressionContext ctx) {
        if (ctx.NUM() != null) {
            return Double.valueOf(ctx.NUM().getText());
        }
        if (ctx.ID() != null) {
            String varName = ctx.ID().getText();
            if (vars.containsKey(varName)) {
                return vars.get(varName);
            }
            throw MissingVariableException.fromVarId(ctx.ID());
        }
        if (ctx.expression().size() == 1) {
            return visit(ctx.expression(0));
        }
        if (ctx.expression().size() == 2) {
            Double leftOperand = visit(ctx.expression(0));
            Double rightOperand = visit(ctx.expression(1));

            if (ctx.multiplOp() != null) {
                if (ctx.multiplOp().DIV() != null && rightOperand == 0.) {
                    return handleDivByZero();
                }
                return ctx.multiplOp().MUL() != null
                        ? leftOperand * rightOperand
                        : leftOperand / rightOperand;
            }
            if (ctx.additionOp() != null) {
                return ctx.additionOp().PLUS() != null
                        ? leftOperand + rightOperand
                        : leftOperand - rightOperand;
            }
        }

        return super.visitExpression(ctx);
    }

    private Double handleDivByZero() {
        throw new RuntimeException("Div by zero");
    }
}
