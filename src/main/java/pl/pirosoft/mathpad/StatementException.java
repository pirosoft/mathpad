package pl.pirosoft.mathpad;

import java.util.ArrayList;
import java.util.List;

public class StatementException extends RuntimeException {

    private final List<SyntaxError> errors = new ArrayList<>();

    StatementException(String message, int charPosition) {
        super(message);

        this.errors.add(new SyntaxError(charPosition, message));
    }

    public StatementException(List<SyntaxError> syntaxErrors) {
        super("Syntax errors found: " + syntaxErrors.size());
        this.errors.addAll(syntaxErrors);
    }

    public List<SyntaxError> getErrors() {
        return errors;
    }
}
