grammar MathPad;

script
    :   (statement)*
    ;

statement
    :   expression
    |   assignment
    ;

expression
    :   NUM
    |   ID
    |   '(' expression ')'
    |   expression multiplOp expression
    |   expression additionOp expression
    ;

multiplOp
    :   MUL | DIV
    ;

additionOp
    :   PLUS|MINUS
    ;

MUL   : '*';
DIV   : '/';
PLUS  : '+';
MINUS : '-';

assignment
    :   ID '=' expression
    ;

ID  : [a-z]+;
NUM : [0-9]+;
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines